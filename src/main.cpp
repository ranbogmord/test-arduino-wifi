#include <Arduino.h>
#include <Preferences.h>
#include <WiFi.h>
#include <WebServer.h>
#include <WifiSetup.h>

#define RESET_PIN 13

WebServer server(80);
WifiSetup wifiSetup(&server, RESET_PIN);
String mode;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RESET_PIN, INPUT);

  Serial.begin(115200);

  wifiSetup.setup();
}

void loop() {
  mode = wifiSetup.tick();
  if (mode == "setup") {
    
  } else {
    Serial.println("In app mode");
    
    digitalWrite(LED_BUILTIN, HIGH);
    delay(500);
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
  }
}
