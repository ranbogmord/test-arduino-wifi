#ifndef WifiSetup_h
#define WifiSetup_h


#if ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include "pins_arduino.h"
  #include "WConstants.h"
#endif

#include <Preferences.h>
#include <WiFi.h>
#include <WebServer.h>

class WifiSetup {
    public:
        WifiSetup(WebServer *server, int forceConfigMode);

        String state();
        String tick();
        void setup();
    private:
        Preferences preferences;
        String ssid;
        String pass;
        WebServer *server;
        int forceConfigModePin;
        String mode;
        String form_content;

        bool test_wifi();
        void handle_save();
        void handle_index();
        void setup_form();
        String render_layout(String body);
};

#endif