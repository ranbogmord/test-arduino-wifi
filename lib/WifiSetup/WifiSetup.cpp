#include "WifiSetup.h"
#include <Preferences.h>
#include <WiFi.h>
#include <WebServer.h>

WifiSetup::WifiSetup(WebServer *webserver, int forceConfigMode) {
    this->forceConfigModePin = forceConfigMode;
    this->server = webserver;
}

String WifiSetup::render_layout(String body) {
    String content = "";
    content += "<!DOCTYPE html>";
    content += "<html lang=\"en\">";
    content += "<head>";
    content += "<meta charset=\"UTF-8\">";
    content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
    content += "<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">";
    content += "<title>WiFi Test</title>";
    content += "</head>";
    content += "<body>";
    content += body;
    content += "</body>";
    content += "</html>";

    return content;
}

void WifiSetup::setup_form() {
  String content = "<h1>TestWiFi Setup</h1>";
  content += "<form action=\"/save\" method=\"post\">";
  content += "<div><label for=\"ssid\">SSID:</label><br>";
  content += "<select id=\"ssid\" name=\"ssid\">";

  int n = WiFi.scanNetworks();
  for (int i = 0; i < n; i++) {
    content += "<option value=\""+ WiFi.SSID(i) +"\">" + WiFi.SSID(i) + "</option>";
    delay(10);
  }

  content += "</select></div>";
  content += "<div><label  for=\"pass\">Password:</label><br>";
  content += "<input type=\"text\" id=\"pass\" name=\"pass\" /></div>";
  content += "<div><input type=\"submit\" value=\"Save\" /></div>";

  content += "<form action=\"/save\" method=\"post\" onsubmit=\"return confirm('Do you really want to reset the configuration?')\">";
  content += "<h2>Reset config</h2>";
  content += "<input type=\"submit\" value=\"Reset\" name=\"reset\">";
  content += "</form>";

  this->form_content = content;
}

void WifiSetup::handle_index() {
    Serial.println("GET /");
    server->send(200, "text/html", render_layout(form_content));
}

void WifiSetup::handle_save() {
    Serial.println("POST /save");
    if (server->hasArg("reset") && server->arg("reset") == "Reset") {
        preferences.clear();
        server->send(200, "text/html", render_layout("Configuration reset, restart the device"));
        return;
    } else {
        if (!server->hasArg("ssid") || !server->hasArg("pass")) {
            server->send(400, "text/html", render_layout("Bad Request"));
            return;
        }

        String ssid = server->arg("ssid");
        String pass = server->arg("pass");

        preferences.putString("ssid", ssid);
        preferences.putString("password", pass);

        server->send(200, "text/html", render_layout("Saved, restart the device"));
        return;
    }
}

void WifiSetup::setup() 
{
    mode = "setup";

    WiFi.disconnect();
    preferences.begin("wifi-setup", false);

    ssid = preferences.getString("ssid", "");
    pass = preferences.getString("password", "-1");

    if (digitalRead(this->forceConfigModePin) == HIGH) {
        mode = "setup";
    } else if (ssid == "" || pass == "-1") {
        mode = "setup";
    } else if (!test_wifi()) {
        mode = "setup";
    } else {
        mode = "app";
    }

    if (mode == "setup") {
        WiFi.mode(WIFI_STA);
        delay(100);
        WiFi.disconnect();
        delay(100);

        Serial.println("Scanning for WiFi networks");
        setup_form();

        server->on("/", [this](){
            this->handle_index();
        });
        server->on("/save", [this](){
            this->handle_save();
        });

        server->begin();

        char * apSsid = "test-wifi";
        WiFi.softAP(apSsid, "test1234");
        delay(100);

        delay(1000);
  
        Serial.print("Local IP: ");
        Serial.println(WiFi.localIP());

        Serial.print("SoftAP IP: ");
        Serial.println(WiFi.softAPIP());
    }
}

bool WifiSetup::test_wifi() {
    WiFi.begin((char *) ssid.c_str(), (char *) pass.c_str());
  
    for (int i = 0; i < 20; i++) {
        if (WiFi.status() == WL_CONNECTED) {
        return true;
        }

        delay(500);
    }

    return false;
}

String WifiSetup::tick() {
    if (mode == "setup") {
        server->handleClient();
    }

    return mode;
}